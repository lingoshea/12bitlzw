#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <lzw_encoder.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>

#define BLOCK_SIZE (4096 * 4)

void usage(const char * proj_name)
{

}

bool read_block(std::string & buffer, int file)
{
    char static_buffer [BLOCK_SIZE] { };
    auto size = read(file, static_buffer, sizeof (static_buffer));
    for (uint64_t i = 0; i < size; i++)
    {
        buffer.push_back(static_buffer[i]);
    }

    if (size > 0)
    {
        return true;
    }

    return false;
}

void write_block(const std::string & buffer, int file)
{
    char static_buffer [BLOCK_SIZE] { };
    for (uint64_t i = 0; i < buffer.size(); i++)
    {
        static_buffer[i] = buffer.at(i);
    }

    write(file, static_buffer, buffer.size());
}


int
main (int argc, char **argv)
{
    int wr2stdut = 0;    // write to stdout
    int rdfstdin = 0;    // read from stdin
    int compress = 0;
    int decompress = 0;
    const char * input_file_path = nullptr;
    const char * output_file_path = nullptr;
    int input_file = 0;
    int output_file = 0;
    std::string buffer;
    int c;

    while (true)
    {
        static struct option long_options[] =
                {
                        {"stdout",          no_argument, nullptr, 's'},
                        {"stdin",           no_argument, nullptr, 'e'},
                        {"compress",        no_argument, nullptr, 'z'},
                        {"decompress",      no_argument, nullptr, 'd'},
                        {"input",           required_argument, nullptr, 'i'},
                        {"output",          required_argument, nullptr, 'o'},
                        {"help",            no_argument, nullptr, 'h'},
                        {nullptr, 0, nullptr, 0}
                };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "sezdi:o:h",
                         long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c)
        {
            case 's':
                wr2stdut = 1;
                break;

            case 'e':
                rdfstdin = 1;
                break;

            case 'h':
                usage(*argv);
                break;

            case 'z':
                compress = 1;
                break;

            case 'd':
                decompress = 1;
                break;

            case 'i':
                input_file_path = optarg;
                break;

            case 'o':
                output_file_path = optarg;
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                return 0;
        }
    }

    if (compress && decompress)
    {
        std::cerr << "Compress and decompress are mutually exclusive!" << std::endl;
        return 1;
    }

    if (input_file_path && rdfstdin)
    {
        std::cerr << "Confused due to specifying reading from stdin while providing an input file path." << std::endl;
        return 1;
    }

    if (rdfstdin)
    {
        input_file = STDIN_FILENO; // stdin
    }

    if (output_file_path && wr2stdut)
    {
        std::cerr << "Confused due to specifying writing to stdout while providing an out file path." << std::endl;
        return 1;
    }

    if (wr2stdut)
    {
        output_file = STDOUT_FILENO; // stdout
    }

    if (input_file_path)
    {
        input_file = open(input_file_path, O_RDONLY);
        if (input_file == -1)
        {
            std::cerr << "Cannot open input file (errno=" << strerror(errno) << ")" << std::endl;
            return 1;
        }
    }

    if (output_file_path)
    {
        output_file = open(output_file_path, O_WRONLY);
        if (output_file == -1)
        {
            std::cerr << "Cannot open input file (errno=" << strerror(errno) << ")" << std::endl;
            return 1;
        }
    }

    x12bit_encoder encoder;

    if (compress)
    {
        while (read_block(buffer, input_file))
        {

        }
    }
}
