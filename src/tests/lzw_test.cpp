#include <lzw_encoder.h>
#include <cstring>

int main()
{
    std::string data = "Hello, World!Hello, World!";
    std::string raw_buffer;
    x12bit_encoder buffer, bank;
    lzw_compress(data, buffer);
    auto code = buffer.encode();
    auto decoded_compress_data = x12bit_decode(code);
    bank.move(decoded_compress_data);
    lzw_decompress(raw_buffer, bank);
    if (raw_buffer == data)
    {
        return 0;
    }

    return 1;
}
