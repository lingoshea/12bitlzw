#include <x12bit_encoder.h>
#include <cstring>

int main()
{
    x12bit_encoder encoder;
    const char * data = "Hello, World!";
//    encoder.push_back(data, strlen(data));
    encoder.push_back(257);
    encoder.push_back(260);
    encoder.push_back(288);
    encoder.push_back(292);
    encoder.push_back(276);
    encoder.push_back(259);
    encoder.push_back(262);
    encoder.push_back(288);
    auto code = encoder.encode();
    auto raw_code = x12bit_decode(code);
    uint16_t off = 0;
    for (auto i : raw_code)
    {
        if (i != encoder.get_data_bank()[off++])
        {
            return 1;
        }
    }
    return 0;
}
