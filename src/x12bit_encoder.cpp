#include <x12bit_encoder.h>

void x12bit_encoder::push_back(uint16_t data)
{
    data_bank.push_back(data);
}

std::vector<char8_t> x12bit_encoder::encode()
{
    std::vector<char8_t> ret;
    char8_t buffer[3];
    uint64_t x223_encoding_ops_count = data_bank.size() / 2;
    uint64_t orphaned_ops = data_bank.size() % 2;

    for (uint64_t i = 0; i < x223_encoding_ops_count; i++)
    {
        encoder_2param2three(buffer, i * 2);

        ret.push_back(buffer[0]);
        ret.push_back(buffer[1]);
        ret.push_back(buffer[2]);
    }

    if (orphaned_ops == 1)
    {
        if (data_bank[x223_encoding_ops_count * 2] > 0xFFF)
        {
            throw encode_error();
        }

        ret.push_back(data_bank[x223_encoding_ops_count * 2] & 0x00FF);
        ret.push_back((data_bank[x223_encoding_ops_count * 2] & 0x0F00) >> 8);
    }

    return ret;
}

void x12bit_encoder::encoder_2param2three(char8_t output_buffer[3], uint64_t starting_offset)
{
    output_buffer[0] = 0;
    output_buffer[1] = 0;
    output_buffer[2] = 0;

    uint16_t x_1 = data_bank[starting_offset], x_2 = data_bank[starting_offset + 1];

    if (x_1 > 0xFFF || x_2 > 0xFFF)
    {
        throw encode_error();
    }

    output_buffer[0] = x_1 & 0x00FF;
    output_buffer[1] = (x_1 & 0x0F00) >> 8;
    output_buffer[1] |= (x_2 & 0x000F) << 4;
    output_buffer[2] = (x_2 & 0x0FF0) >> 4;
}

void x12bit_encoder::push_back(const char *data, uint64_t count)
{
    for (uint64_t i = 0; i < count; i++)
    {
        this->push_back(data[i]);
    }
}

/// decoding uint16_t to uint8_t
/** @param code code
 *  @param output_buffer output buffer
 *  @param starting_offset decode starting offset
 *  @return none **/
void decoder_3param2two(std::vector < char8_t > & code, uint16_t output_buffer[2], uint64_t starting_offset)
{
    output_buffer[0] = 0;
    output_buffer[1] = 0;

    char8_t x_1 = code[starting_offset],
            x_2 = code[starting_offset + 1],
            x_3 = code[starting_offset + 2];

    output_buffer[0] =  x_1; // 1 - 8
    output_buffer[0] |= (x_2 & 0x000F) << 8; // 9 - 12
    output_buffer[1] =  (x_2 & 0x00F0) >> 4; // 1 - 4
    output_buffer[1] |= (x_3 & 0x00FF) << 4; // 5 - 12
}

std::vector < uint16_t > x12bit_decode(std::vector < char8_t > & code)
{
    std::vector<uint16_t> ret;
    uint16_t buffer[2];
    uint64_t x322_encoding_ops_count = code.size() / 3;
    uint64_t orphaned_ops = code.size() % 3;

    for (uint64_t i = 0; i < x322_encoding_ops_count; i++)
    {
        decoder_3param2two(code, buffer, i * 3);

        ret.push_back(buffer[0]);
        ret.push_back(buffer[1]);
    }

    if (orphaned_ops == 2)
    {
        uint16_t data = code[x322_encoding_ops_count * 3];
        data |= code[x322_encoding_ops_count * 3 + 1] << 8;
        ret.push_back(data);
    }

    return ret;
}
