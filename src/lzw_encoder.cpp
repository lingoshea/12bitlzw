#include <lzw_encoder.h>
#include <string>
#include <map>

/// Compress a string to a list of output symbols.
void compress(const std::string &uncompressed, std::vector < uint16_t > & result)
{
    // Build the dictionary.
    int dictSize = 256;
    std::map < std::string, int > dictionary;
    for (int i = 0; i < 256; i++)
    {
        dictionary[std::string(1, (char) i)] = i;
    }

    std::string t_string;
    for (char char_c : uncompressed)
    {
        std::string wc = t_string + char_c;
        if (dictionary.count(wc))
        {
            t_string = wc;
        }
        else
        {
            result.push_back(dictionary[t_string]);
            // Add wc to the dictionary.
            dictionary[wc] = dictSize++;
            t_string = std::string(1, char_c);
        }
    }

    // Output the code for w.
    if (!t_string.empty())
    {
        result.push_back(dictionary[t_string]);
    }
}

class bad_compress_error : std::exception
{
public:
    [[nodiscard]] const char * what() const noexcept override { return "Bad compressed k"; }
};


/// Decompress a list of output ks to a string.
void decompress(std::string & buffer, const std::vector < uint16_t > & code)
{
    // Build the dictionary.
    auto begin = code.begin();
    auto end = code.end();
    int dictSize = 256;
    std::map < int, std::string > dictionary;
    for (int i = 0; i < 256; i++)
    {
        dictionary[i] = std::string(1, (char) i);
    }

    std::string t_string(1, (char)*begin++);
    std::string result = t_string;
    std::string entry;
    for (;begin != end; begin++)
    {
        int k = *begin;
        if (dictionary.count(k))
        {
            entry = dictionary[k];
        }
        else if (k == dictSize)
        {
            entry = t_string + t_string[0];
        }
        else
        {
            throw bad_compress_error();
        }

        result += entry;

        // Add w+entry[0] to the dictionary.
        dictionary[dictSize++] = t_string + entry[0];

        t_string = entry;
    }

    buffer = result;
}

x12bit_encoder & lzw_compress(const std::string & data, x12bit_encoder & buffer)
{
    std::vector < uint16_t > result;
    compress(data, result);
    buffer.move(result);
    return buffer;
}

void lzw_decompress(std::string & buffer, x12bit_encoder & code)
{
    decompress(buffer, code.get_data_bank());
}
