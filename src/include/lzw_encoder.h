#ifndef INC_12BITLZW_LZW_ENCODER_H
#define INC_12BITLZW_LZW_ENCODER_H

#include <x12bit_encoder.h>
#include <map>
#include <string>

/// LZW compress
/** @param data data for compression
 *  @param buffer for storing compressed data
 *  @return Returns a 12bit encoder **/
x12bit_encoder & lzw_compress(const std::string & data, x12bit_encoder & buffer);

/// LZW compress
/** @param buffer buffer for storage
 *  @param code compressed code
 *  @return none **/
void lzw_decompress(std::string & buffer, x12bit_encoder & code);

#endif //INC_12BITLZW_LZW_ENCODER_H
