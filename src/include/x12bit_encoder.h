#ifndef INC_12BITLZW_X12BIT_ENCODER_H
#define INC_12BITLZW_X12BIT_ENCODER_H

#include <cstdint>
#include <utility>
#include <vector>
#include <exception>

/// throw this class when data exceeds 12bit data width
class encode_error : public std::exception
{
public:
    [[nodiscard]] const char * what() const noexcept override { return "Data exceeding 12bit width"; }
};

/// 12bit encoder
class x12bit_encoder
{
    std::vector < uint16_t > data_bank; // data

    /// encode 3 params in data_bank into 2 char_t
    /** @param output_buffer output buffer
     *  @param starting_offset encoder starting offset (!! doesn't check availability !!)
     *  @return none **/
    void encoder_2param2three(char8_t output_buffer[3], uint64_t starting_offset);

public:

    x12bit_encoder() = default;

    /// move _data_bank to data_bank
    /** @param _data_bank data_bank that wish to be replaced with **/
    void move (const std::vector < uint16_t > & _data_bank) { data_bank = _data_bank; }

    /// get data
    [[nodiscard]] std::vector < uint16_t > get_data_bank() const { return data_bank; }

    /// append data into data_bank
    /** @param data data pending for writing
     *  @return none **/
    void push_back(uint16_t data);

    /// append data into data_bank
    /** @param data data pending for writing
     *  @param count 16bit data count
     *  @return none **/
    void push_back(const char * data, uint64_t count);

    /// encode data_bank
    /** @param none
     *  @return encoded char8_t vector **/
    std::vector < char8_t > encode();

    x12bit_encoder & operator=(const x12bit_encoder&&) = delete;
};

/// decode x12bit code
/**  @param code code for decoding
  *  @return decoded uint16_t vector **/
std::vector < uint16_t > x12bit_decode(std::vector < char8_t > & code);

#endif //INC_12BITLZW_X12BIT_ENCODER_H
